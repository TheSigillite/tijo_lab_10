package pl.edu.pwsztar.service.impl;


import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.figures.bishop.MoveBishop;
import pl.edu.pwsztar.service.ChessService;
@Service
public class ChessServiceImpl implements ChessService {

    private final MoveBishop moveBishop;

    public ChessServiceImpl(MoveBishop moveBishop) {
        this.moveBishop = moveBishop;
    }

    @Override
    public boolean checkBishop(FigureMoveDto figureMoveDto) {
        return moveBishop.move(figureMoveDto.getStart().charAt(0)
                ,figureMoveDto.getDestination().charAt(0)
                ,figureMoveDto.getStart().charAt(2)
                ,figureMoveDto.getDestination().charAt(2));
    }
}
