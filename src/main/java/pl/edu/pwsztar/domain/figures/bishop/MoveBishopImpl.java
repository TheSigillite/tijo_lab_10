package pl.edu.pwsztar.domain.figures.bishop;

import org.springframework.stereotype.Component;

@Component
public class MoveBishopImpl implements MoveBishop {
    @Override
    public boolean move(int colfrom, int colto, int rowfrom, int rowto) {
        if(colfrom == colto){
            return false;
        }
        if(rowfrom == rowto){
            return false;
        }
        return Math.abs(colto - colfrom) == Math.abs(rowto - rowfrom);
    }
}
