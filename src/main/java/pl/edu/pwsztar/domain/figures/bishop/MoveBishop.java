package pl.edu.pwsztar.domain.figures.bishop;

public interface MoveBishop {
    boolean move(int colfrom, int colto, int rowfrom, int rowto);
}
